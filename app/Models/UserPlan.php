<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'plan_id',
        'product_id',
        'is_default',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
