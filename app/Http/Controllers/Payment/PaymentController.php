<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\UserPlan;
use Auth;
use Illuminate\Http\Request;
use Laravel\Cashier\Exceptions\IncompletePayment;

class PaymentController extends Controller
{
    protected $stripe;

    public function __construct()
    {
        \Stripe\Stripe::setApiKey(\Config::get('stripe.stripe_secret'));

        $this->stripe = new \Stripe\StripeClient(
            \Config::get('stripe.stripe_secret')
        );

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('payments.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = Auth::User();
            $creditCard = $this->creditCard($user);

            $product = $this->stripe->products->create([
                'name' => 'user_product_' . $user->id . \Str::random(10),
            ]);

            $amount =
            $request->hour *
            $request->day *
            $request->month *
                0.65;

            $amount = number_format($amount, 2);

            $plan = $this->stripe->plans->create([
                'amount' => $amount * 100,
                'currency' => 'usd',
                'interval' => 'month',
                'product' => $product->id,
            ]);

            $userPlan = UserPlan::create([
                'user_id' => $user->id,
                'plan_id' => $plan->id,
                'product_id' => $product->id,
            ]);

            $trialDays = 7;

            $userPlans = UserPlan::all();

            if (count($userPlans) > 1) {
                $trialDays = 10;
            }

            $user->newSubscription($plan->id, $plan->id)
                ->trialDays($trialDays)
                ->create($creditCard->id);

            $user->subscription($plan->id)->noProrate()->updateQuantity(1);

            $customer = $user->createOrGetStripeCustomer();
            $subscription = $customer->subscriptions;
        } catch (IncompletePayment $e) {

            return redirect()->route(
                'cashier.payment',
                [$e->payment->id, 'redirect' => \Redirect::back()]
            );
        }

        return view('payments.view', [
            'subscription' => $subscription,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function creditCard($user)
    {
        $request = app('request');
        $customer = $user->createOrGetStripeCustomer();
        $customer = \Stripe\Customer::retrieve($customer->id);
        $smonth = explode("/", $request->smonth);
        $token = \Stripe\Token::create([
            "card" => array(
                "number" => $request->scard,
                "exp_month" => $smonth[0],
                "exp_year" => $smonth[1],
                "cvc" => $request->scvc,
            ),
        ]);

        return $customer->sources->create(array("source" => $token));
    }

    public function unsubscribe()
    {

        $user = Auth::User();

        $userPlan = UserPlan::where('user_id', $user->id)
            ->where('is_default', 1)
            ->first();
        $userPlan->is_default = 0;
        $userPlan->update();

        $user->subscription($userPlan->plan_id)->cancel();

        if ($user->subscription($userPlan->plan_id)->onTrial()) {
            //
        }

        return view('payments.unsubscribe');

    }
}
