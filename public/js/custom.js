$(document).ready(function() {
    var hour = $("#hour");
    var day = $("#day");
    var month = $("#month");
    var total = $("#total");

    var checkout = $("#checkout");
    var plan = $("#plan");
    var previous = $("#previous");
    var checkoutPayment = $("#checkout-payment");
    var payNow = $("#pay-now");

    $("#form").validate({
        rules: {
            scard: {
                required: true,
                minlength: 16,
                maxlength: 16
            },
            smonth: {
                required: true,
                minlength: 5,
                maxlength: 5
            },
            scvc: {
                required: true
            }
        },
        messages: {
            hour: {
                required: "Please select an hours."
            },
            day: {
                required: "Please select an days."
            },
            month: {
                required: "Please select an months."
            },
            scard: {
                required: "Card No. is required.",
                minlength: "Length must be 16.",
                maxlength: "Length must be 16."
            },
            smonth: {
                required: "Month is required.",
                minlength: "Length must be 5.",
                maxlength: "Length must be 5."
            },
            scvc: {
                required: "CVC is required."
            }
        }
    });

    function compute() {
        if (hour.val() != "" && day.val() != "" && month.val() != "") {
            let t =
                parseFloat(hour.val()) *
                parseFloat(day.val()) *
                parseFloat(month.val()) *
                0.65;
            total.text("$" + t.toFixed(2));
        }
    }
    $("#hour, #day, #month").on("change", function() {
        compute();
    });

    checkout.on("click", function() {
        if (
            $("[name='hour']").valid() &&
            $("[name='day']").valid() &&
            $("[name='month']").valid()
        ) {
            $(this).hide();
            plan.hide();
            previous.show();
            checkoutPayment.show();
            payNow.show();
        }
    });

    previous.on("click", function() {
        $(this).hide();
        checkoutPayment.hide();
        payNow.hide();
        plan.show();
        checkout.show();
    });
    checkoutPayment.on("click", function() {
        if ($("#form").valid()) {
            $("#form").submit();
        }
    });

    $("#smonth").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (
            e.which != 47 &&
            e.which != 8 &&
            e.which != 0 &&
            (e.which < 48 || e.which > 57)
        ) {
            //display error message
            return false;
        }
    });
});
