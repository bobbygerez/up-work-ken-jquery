@extends('layouts.app')
@section('content')
    <h1>Stripe Payment</h1>
        <form id="form" method="post" action="/payments">
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
        <div class="row" id="plan">
            <div class="col-8">
                    <div class="form-group">
                        <label for="hour">No. of Hours</label>
                        <select class="form-control required" id="hour" name="hour" >
                            <option value="">Choose...</option>
                            <option value="1">1 hour</option>
                            @for($i = 2; $i <= 24; ++$i)
                                <option value="{{ $i }}">{{ $i }} hours</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="day">No. of days</label>
                        <select class="form-control required" id="day" name="day">
                            <option value="">Choose...</option>
                            <option value="1">1 day</option>
                            @for($i = 2; $i <= 7; ++$i)
                                <option value="{{ $i }}">{{ $i }} days</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="month">Duration</label>
                        <select class="form-control required" id="month" name="month">
                            <option value="">Choose...</option>
                            <option value="4">1 month</option>
                            <option value="12">3 months</option>
                            <option value="24">6 months</option>
                            <option value="48">1 year</option>
                        </select>
                    </div>

            </div>
            <div class="col-4">
                <ul class="promo-code">
                    <li>
                        <span>PROMO CODE:</span> $0
                    </li>
                    <li>
                        <span>Total (USD)</span>  <strong id="total">$0</strong>
                    </li>
                </ul>

            </div>

        </div>
        <div class="row" id="pay-now">
            <div class="col-6">
                <div class="form-group">
                    <label for="scard">Card No.</label>
                    <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control" id="scard" value="" placeholder="Enter Card No." name="scard" maxlength="16">
                </div>
                 <div class="form-group">
                    <label for="smonth">Month</label>
                    <input type="text" class="form-control" id="smonth" value="" placeholder="MM/YY" name="smonth" maxlength="5">
                </div>
                 <div class="form-group">
                    <label for="scvc">CVC</label>
                    <input type="number" class="form-control" id="scvc" value="" placeholder="CVC" name="scvc">
                </div>
            </div>

        </div>
            <button type="button" class="btn btn-primary" id="checkout">Continue to Checkout</button>
            <button type="button" class="btn btn-primary" id="previous">Previous</button>
            <button type="button" class="btn btn-success" id="checkout-payment">Payment</button>
        </form>
@endsection
