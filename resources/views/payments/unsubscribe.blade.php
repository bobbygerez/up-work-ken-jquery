@extends('layouts.app')
@section('content')
       <div class="alert alert-success" role="alert">
            You have successfully unsubscribe
        </div>
        <br />
         <a href="logout" class="btn btn-primary active" role="button" aria-pressed="true">Logout</a>
@endsection
